#! /bin/sh

cd "$(dirname "$0")" &&
if [ -d builddir ]
then
	meson setup --reconfigure --pkg-config-path "$PKG_CONFIG_PATH" builddir "$@"
else
	meson --pkg-config-path "$PKG_CONFIG_PATH" builddir "$@"
fi &&
cd builddir &&
ninja &&
ninja test &&
echo '=== SUCCESS ===' ||
echo '===  ERROR  ==='
