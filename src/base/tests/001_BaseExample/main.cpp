#include <asiopp.hpp>
#include <iostream>

using ::asiopp::base::AsyncFunc;
using ::asiopp::base::Main;
using ::std::cout;
using ::std::endl;

AsyncFunc<int> test_1_1(int input) {
	co_return input;
}

// CoFuture<int> test_1(MainLoop &ml, int input) {
//	co_return test_1_1(input).result();
// }

int main() {
	Main ml;
	cout << ml.run(test_1_1(10)) << endl;
	return 0;
}
