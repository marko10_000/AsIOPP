#include "asiopp/base/basetypes.hpp"
#include "asiopp/intern/atomicPointer.hpp"
#include <asiopp/base/main.hpp>

namespace {
	class RealBaseMain final : public ::asiopp::base::intern::BaseMain {
		private:
			class Accessor : private ::asiopp::base::intern::BaseCoRoutine {
				public:
					static bool runOther(::asiopp::base::intern::BaseCoRoutine &bcr) {
						return ::RealBaseMain::Accessor::_runOther(bcr);
					}
			};

		public:
			~RealBaseMain() {}

			void runMain(::asiopp::base::intern::BaseCoRoutine &func) override {
				while (!Accessor::runOther(func)) {
				}
			}
	};
} // namespace

::asiopp::intern::AtomicSharedPointer<::asiopp::base::intern::BaseMain> asiopp::base::intern::BaseMain::genMain() {
	return new RealBaseMain();
}
