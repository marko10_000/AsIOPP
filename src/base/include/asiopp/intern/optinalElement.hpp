#pragma once
#include <type_traits>
#include <utility>

namespace asiopp::intern {
	template <class CONTENT>
	struct OptionalElement {
		public:
			char content[sizeof(CONTENT)] = {};
			bool isInit = false;

			~OptionalElement() {
				this->destroy();
			}

			void destroy() {
				if (this->isInit) {
					this->get()->~CONTENT();
				}
				this->isInit = false;
			}

			CONTENT *get() {
				if (isInit) {
					return static_cast<CONTENT *>(static_cast<void *>(this->content));
				} else {
					return nullptr;
				}
			}
			const CONTENT *get() const {
				if (isInit) {
					return static_cast<const CONTENT *>(static_cast<void *>(this->content));
				} else {
					return nullptr;
				}
			}

			template <class... ARGUMENTS>
			requires ::std::is_constructible_v<CONTENT, ARGUMENTS...>
				CONTENT *make_new(ARGUMENTS... arguments) {
				auto result = this->pos_for_new();
				new (result) CONTENT(::std::forward<ARGUMENTS>(arguments)...);
				return result;
			}

			CONTENT *pos_for_new() {
				if (isInit) {
					return nullptr;
				} else {
					this->isInit = true;
					return this->get();
				}
			}
	};
	template <>
	struct OptionalElement<void> {};
} // namespace asiopp::intern
