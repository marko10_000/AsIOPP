#pragma once
#include <asiopp/intern/atomicLocks.hpp>
#include <atomic>
#include <cstddef>

namespace asiopp::intern {
	template <class TYPE>
	class AtomicSharedPointer {
		private:
			::std::atomic<::std::atomic<size_t> *> _counter = nullptr;
			::std::atomic<TYPE *> _value = nullptr;
			::asiopp::intern::AtomicLock<true> _lock = {};

		public:
			AtomicSharedPointer() {}
			AtomicSharedPointer(TYPE *value) {
				this->_counter.store(new ::std::atomic<size_t>(1));
				this->_value.store(value);
			}
			AtomicSharedPointer(const AtomicSharedPointer &ptr) {
				const_cast<AtomicSharedPointer &>(ptr)._lock.locked([this, &ptr]() -> void {
					this->_counter.store(ptr._counter.load());
					this->_value.store(ptr._value.load());
					if (this->_counter.load() != nullptr) {
						this->_counter.load()->fetch_add(1);
					}
				});
			}
			AtomicSharedPointer(AtomicSharedPointer &&ptr) {
				ptr._lock.locked([this, &ptr]() -> void {
					this->_counter.store(ptr._counter.exchange(nullptr));
					this->_value.store(ptr._value.exchange(nullptr));
				});
			}
			~AtomicSharedPointer() {
				if (this->_counter.load() != nullptr) {
					if (this->_counter.load()->fetch_sub(1) <= 1) {
						delete this->_counter.load();
						delete this->_value.load();
					}
				}
			}

			AtomicSharedPointer &operator=(const AtomicSharedPointer &ptr) {
				::std::pair<::std::atomic<size_t> *, TYPE *> tmp = const_cast<AtomicSharedPointer &>(ptr)._lock.locked([&ptr]() -> ::std::pair<::std::atomic<size_t> *, TYPE *> {
					::std::atomic<size_t> *tmp1 = ptr._counter.load();
					TYPE *tmp2 = ptr._value.load();
					if (tmp1 != nullptr) {
						tmp1->fetch_add(1);
					}
					return ::std::pair<::std::atomic<size_t> *, TYPE *>(tmp1, tmp2);
				});
				tmp = this->_lock.locked([this, tmp]() -> ::std::pair<::std::atomic<size_t> *, TYPE *> {
					::std::pair<::std::atomic<size_t> *, TYPE *> res(this->_counter.exchange(tmp.first), this->_value.exchange(tmp.second));
					return res;
				});
				if (tmp.first != nullptr) {
					if (tmp.first->fetch_sub(1) <= 1) {
						delete tmp.first;
						delete tmp.second;
					}
				}
				return *this;
			}
			AtomicSharedPointer &operator=(AtomicSharedPointer &&ptr) {
				::std::pair<::std::atomic<size_t> *, TYPE *> tmp = ptr._lock.locked([&ptr]() -> ::std::pair<::std::atomic<size_t> *, TYPE *> {
					return ::std::pair<::std::atomic<size_t> *, TYPE *>(ptr._counter.exchange(nullptr), ptr._value.exchange(nullptr));
				});
				tmp = this->_lock.locked([this, tmp]() -> ::std::pair<::std::atomic<size_t> *, TYPE *> {
					::std::pair<::std::atomic<size_t> *, TYPE *> res(this->_counter.exchange(tmp.first), this->_value.exchange(tmp.second));
					return res;
				});
				if (tmp.first != nullptr) {
					if (tmp.first->fetch_sub(1) <= 1) {
						delete tmp.first;
						delete tmp.second;
					}
				}
				return *this;
			}
			TYPE *operator->() {
				return this->get();
			}
			const TYPE *operator->() const {
				return this->get();
			}

			TYPE *get() {
				return this->_value.load();
			}
			const TYPE *get() const {
				return this->_value.load();
			}
	};
} // namespace asiopp::intern
