#pragma once
#include <atomic>
#include <cstddef>
#include <type_traits>

namespace asiopp::intern {
	template <class TYPE>
	concept AtomicLockFunc = ::std::is_invocable_v<TYPE>;
	template <class TYPE>
	concept AtomicLockFuncV = AtomicLockFunc<TYPE> && ::std::is_same_v < ::std::invoke_result_t<TYPE>,
	void > ;
	template <class TYPE>
	concept AtomicLockFuncR = AtomicLockFunc<TYPE> &&(!::std::is_same_v<::std::invoke_result_t<TYPE>, void>);

	/**
	 * Locks the given lock and then calls the given function.
	 * @tparam CONFIG Lock configuration
	 * @tparam CALLER Function type to call
	 * @tparam LOCK Lock type to lock on
	 * @param lock The lock to use use
	 * @param caller The function object to call
	 * @return Return value of the called function
	 */
	template <class CONFIG, AtomicLockFuncV CALLER, class LOCK>
	inline void atomicLocked(LOCK &lock, CALLER &&caller) {
		CONFIG::lock(lock);
		try {
			caller();
			CONFIG::unlock(lock);
			return;
		} catch (...) {
			CONFIG::unlock(lock);
			throw;
		}
	}
	template <class CONFIG, AtomicLockFuncR CALLER, class LOCK>
	inline ::std::invoke_result_t<CALLER> atomicLocked(LOCK &lock, CALLER &&caller) {
		CONFIG::lock(lock);
		try {
			::std::invoke_result_t<CALLER> res = caller();
			CONFIG::unlock(lock);
			return res;
		} catch (...) {
			CONFIG::unlock(lock);
			throw;
		}
	}

	/**
	 * A busy waiting atomic lock.
	 * It's starvation free as long as every successfull lock will be freed with an unlock.
	 *
	 * @tparam FENCES When true thread fences will be acquired/release when locked/unlocked
	 */
	template <bool FENCES = true>
	class AtomicLock final {
		private:
			::std::atomic<size_t> _counter = 0;
			::std::atomic<size_t> _access = 0;

		public:
			/**
			 * Configuration for atomicLocked.
			 * Doesn't support recursice locking.
			 *
			 * Usage: `atomicLocked<AtomicLocked::Conf>(lock, func);`
			 */
			struct Conf {
				public:
					static void lock(AtomicLock &lock) {
						lock.lock();
					}
					static void unlock(AtomicLock &lock) {
						lock.unlock();
					}
			};

			/**
			 * Construct a new Atomic Lock object
			 */
			AtomicLock() {}
			AtomicLock(const AtomicLock &) = delete;
			AtomicLock(AtomicLock &&) = delete;
			/**
			 * Destroy the Atomic Lock object
			 */
			~AtomicLock() {}

			AtomicLock &operator=(AtomicLock &) = delete;
			AtomicLock &operator=(AtomicLock &&) = delete;

			/**
			 * Locks the lock (single access).
			 * Thread fence will be acquired when FENCES is set.
			 */
			void lock() {
				size_t own = this->_counter.fetch_add(1);
				while (this->_access.load() != own) {
				}
				if constexpr (FENCES) {
					::std::atomic_thread_fence(::std::memory_order_acquire);
				}
			}
			/**
			 * Unlocks the lock.
			 * Thread fence will be released when FENCES is set.
			 */
			void unlock() {
				if constexpr (FENCES) {
					::std::atomic_thread_fence(::std::memory_order_release);
				}
				this->_access.fetch_add(1);
			}

			template <AtomicLockFuncV FUNC>
			void locked(FUNC func) {
				atomicLocked<AtomicLock::Conf>(*this, func);
			}
			template <AtomicLockFuncR FUNC>
			::std::invoke_result_t<FUNC> locked(FUNC func) {
				return atomicLocked<AtomicLock::Conf>(*this, func);
			}
	};

	template <bool FENCES = true>
	struct AtomicRWLock final {
		private:
			enum struct State : uint8_t {
				READERS = 0,
				WRITERS_WAITING = 1,
				WRITER = 2,
				READERS_CLEANUP = 3
			};

			AtomicLock<false> _stateLock = {};
			::std::atomic<State> _state = State::READERS;
			::std::atomic<size_t> _readers = 0;
			::std::atomic<size_t> _readersWaiting = 0;
			AtomicLock<false> _writerOrder = {};

		public:
			struct ConfRead {
				public:
					static void lock(AtomicRWLock &lock) {
						lock.lockRead();
					}
					static void unlock(AtomicRWLock &lock) {
						lock.unlockRead();
					}
			};
			struct ConfWrite {
				public:
					static void lock(AtomicRWLock &lock) {
						lock.lockWrite();
					}
					static void unlock(AtomicRWLock &lock) {
						lock.unlockWrite();
					}
			};

			AtomicRWLock() {}
			AtomicRWLock(const AtomicRWLock &) = delete;
			AtomicRWLock(AtomicRWLock &&) = delete;
			~AtomicRWLock() {}

			AtomicRWLock &operator=(const AtomicRWLock &) = delete;
			AtomicRWLock &operator=(AtomicRWLock &&) = delete;

			void lockRead() {
				bool waiting = false;
				this->_stateLock.locked([this, &waiting]() -> void {
					switch (this->_state.load()) {
						case State::READERS_CLEANUP:
						case State::READERS:
							this->_readers.fetch_add(1);
							waiting = false;
							break;
						case State::WRITERS_WAITING:
						case State::WRITER:
							this->_readersWaiting.fetch_add(1);
							waiting = true;
							break;
					}
				});
				while (waiting) {
					switch (this->_state.load()) {
						case State::READERS_CLEANUP:
							this->_readers.fetch_add(1);
							this->_readersWaiting.fetch_sub(1);
							waiting = false;
							break;
						case State::READERS:
							throw; // Illegal state
						case State::WRITERS_WAITING:
						case State::WRITER:
							break; // Waiting
					}
				}
				if constexpr (FENCES) {
					::std::atomic_thread_fence(::std::memory_order_acquire);
				}
			}
			void lockWrite() {
				this->_writerOrder.lock();
				this->_stateLock.locked([this]() -> void {
					if (this->_state.load() != State::READERS) {
						throw; // Illegal state
					}
					this->_state.store(State::WRITERS_WAITING);
				});
				while (this->_readers.load() != 0) {
				}
				this->_state.store(State::WRITER);
				if constexpr (FENCES) {
					::std::atomic_thread_fence(::std::memory_order_acquire);
				}
			}
			void unlockRead() {
				if constexpr (FENCES) {
					::std::atomic_thread_fence(::std::memory_order_release);
				}
				this->_readers.fetch_sub(1);
			}
			void unlockWrite() {
				if constexpr (FENCES) {
					::std::atomic_thread_fence(::std::memory_order_release);
				}
				this->_stateLock.locked([this]() -> void {
					if (this->_state.load() != State::WRITER) {
						throw; // Illegal state
					}
					this->_state.store(State::READERS_CLEANUP);
				});
				while (this->_readersWaiting.load() != 0) {
				}
				this->_state.store(State::READERS);
				this->_writerOrder.unlock();
			}

			template <class FUNC>
			auto readLocked(FUNC &&func) {
				return atomicLocked<AtomicRWLock::ConfRead>(*this, func);
			}
			template <class FUNC>
			auto writeLocked(FUNC &&func) {
				return atomicLocked<AtomicRWLock::ConfWrite>(*this, func);
			}
	};
} // namespace asiopp::intern
