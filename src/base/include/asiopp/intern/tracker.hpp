#pragma once
#include <concepts>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace asiopp::intern {
	template <class VALUE>
	requires ::std::movable<VALUE> && ::std::is_move_constructible_v<VALUE>
	struct Tracked final {
		public:
			struct Tracker;

		private:
			Tracker *_tracker = nullptr;
			VALUE _value;

		public:
			struct Tracker final {
				private:
					Tracked *_tracked = nullptr;

					Tracker(Tracked &tracked) : _tracked(&tracked) {}

				public:
					Tracker() {}
					Tracker(const Tracker &) = delete;
					Tracker(Tracker &&tracker) {
						this->_tracked = tracker._tracked;
						if (this->_tracked != nullptr) {
							this->_tracked->_tracker = this;
						}
						tracker._tracked = nullptr;
					}
					~Tracker() {
						if (this->_tracked != nullptr) {
							this->_tracked->_tracker = nullptr;
						}
					}

					Tracker &operator=(const Tracker &) = delete;
					Tracker &operator=(Tracker &&tracker) {
						Tracked *tmp = tracker._tracked;
						tracker._tracked = nullptr;
						if (tmp != nullptr) {
							tmp->_tracker = this;
						}
						{
							auto tmp2 = this->_tracked;
							this->_tracked = tmp;
							tmp = tmp2;
						}
						if (tmp != nullptr) {
							tmp->_tracker = nullptr;
						}
						return *this;
					}

					operator bool() const {
						return this->_tracked != nullptr;
					}

					VALUE *operator->() {
						auto tmp = this->get();
						if (tmp != nullptr) {
							return tmp;
						} else {
							throw ::std::runtime_error("Tracked not set.");
						}
					}
					const VALUE *operator->() const {
						auto tmp = this->get();
						if (tmp != nullptr) {
							return tmp;
						} else {
							throw ::std::runtime_error("Tracked not set.");
						}
					}

					VALUE *get() {
						if (this->_tracked != nullptr) {
							return &(this->_tracked->_value);
						} else {
							return nullptr;
						}
					}
					const VALUE *get() const {
						if (this->_tracked != nullptr) {
							return &(this->_tracked->_value);
						} else {
							return nullptr;
						}
					}

					friend ::asiopp::intern::Tracked<VALUE>;
			};

			Tracked() {}
			Tracked(const Tracked &) = delete;
			Tracked(Tracked &&tracked) {
				this->_tracker = tracked._tracker;
				if (this->_tracker != nullptr) {
					this->_tracker->_tracked = this;
				}
				tracked._tracker = nullptr;
			}
			~Tracked() {
				if (this->_tracker != nullptr) {
					this->_tracker->_tracked = nullptr;
				}
			}

			Tracked &operator=(const Tracked &) = delete;
			Tracked &operator=(Tracked &&tracked) {
				this->value = ::std::move(tracked.value);
				Tracker *tmp = tracked._tracker;
				tracked._tracker = nullptr;
				if (tmp != nullptr) {
					tmp->_tracked = this;
				}
				::std::swap<Tracker *&>(tmp, this->_tracker);
				if (tmp != nullptr) {
					tmp->_tracked = nullptr;
				}
				return *this;
			}

			VALUE *operator->() {
				return this->get();
			}
			const VALUE *operator->() const {
				return this->get();
			}

			VALUE *get() {
				return &(this->_value);
			}
			const VALUE *get() const {
				return &(this->_value);
			}
			Tracker getTracker() {
				Tracker tracker;
				if (this->_tracker != nullptr) {
					this->_tracker->_tracked = nullptr;
				}
				tracker._tracked = this;
				return tracker;
			}

			friend ::asiopp::intern::Tracked<VALUE>::Tracker;
	};
	template <class VALUE>
	requires(::std::movable<VALUE> && ::std::is_move_constructible_v<VALUE>) || ::std::is_integral_v<VALUE> using Tracker = typename Tracked<VALUE>::Tracker;
} // namespace asiopp::intern
