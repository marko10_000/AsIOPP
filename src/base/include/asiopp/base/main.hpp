#pragma once
#include <asiopp/base/basetypes.hpp>
#include <asiopp/intern/atomicPointer.hpp>
#include <atomic>
#include <concepts>
#include <memory>

namespace asiopp::base {
	namespace intern {
		class BaseMain {
			public:
				virtual ~BaseMain() {}

				virtual void runMain(::asiopp::base::intern::BaseCoRoutine &func) = 0;

				static ::asiopp::intern::AtomicSharedPointer<::asiopp::base::intern::BaseMain> genMain();
		};
	} // namespace intern

	class Main final {
		private:
			::asiopp::intern::AtomicSharedPointer<::asiopp::base::intern::BaseMain> _baseMain;

		public:
			Main() : _baseMain(::asiopp::base::intern::BaseMain::genMain()) {}
			Main(const Main &main) : _baseMain(main._baseMain) {}
			Main(Main &&main) : _baseMain(::std::move(main._baseMain)) {}
			~Main() {}

			Main &operator=(const Main &main) {
				this->_baseMain = main._baseMain;
				return *this;
			}
			Main &operator=(Main &&main) {
				this->_baseMain = ::std::move(main._baseMain);
				return *this;
			}

			template <::std::movable RESULT>
			RESULT run(::asiopp::base::AsyncFunc<RESULT> &&func) {
				this->_baseMain->runMain(func);
				return func.getResult();
			}
	};
} // namespace asiopp::base
