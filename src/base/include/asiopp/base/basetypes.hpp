#pragma once
#include <asiopp/intern/optinalElement.hpp>
#include <asiopp/intern/tracker.hpp>
#include <concepts>
#include <type_traits>

#ifndef __clang__
#include <coroutine>
#else
#include <experimental/coroutine>
#endif

namespace asiopp::base {
	namespace intern {
#ifndef __clang__
		template <class T>
		using coroutine_handle = ::std::coroutine_handle<T>;
		using suspend_never = ::std::suspend_never;
#else
		template <class T>
		using coroutine_handle = ::std::experimental::coroutine_handle<T>;
		using suspend_never = ::std::experimental::suspend_never;
#endif

		template <class TARGET>
		struct SimplePromisType final {
			private:
				::asiopp::intern::Tracker<typename TARGET::_Container> _tracker = {};

			public:
				::std::enable_if_t<(!::std::is_same_v<typename TARGET::_RESULT_T, void>), void> return_value(typename TARGET::_RESULT_T res) {
					this->_tracker->result.make_new(std::move(res));
				}
				TARGET get_return_object() {
					TARGET result(::asiopp::base::intern::coroutine_handle<SimplePromisType<TARGET>>::from_promise(*this));
					this->_tracker = ::std::move(result._data.getTracker());
					return result;
				}
				::asiopp::base::intern::suspend_never initial_suspend() {
					this->_tracker->done = false;
					return {};
				}
				::asiopp::base::intern::suspend_never final_suspend() noexcept {
					this->_tracker->done = true;
					return {};
				}
				void unhandled_exception() {}
		};

		class BaseCoRoutine {
			protected:
				virtual bool _run() = 0;

				static bool _runOther(BaseCoRoutine &co) {
					return co._run();
				}

			public:
				BaseCoRoutine() {}
				virtual ~BaseCoRoutine() {}
		};
	} // namespace intern

	template <::std::movable RESULT>
	class AsyncFunc : public ::asiopp::base::intern::BaseCoRoutine {
		public:
			using promise_type = typename ::asiopp::base::intern::SimplePromisType<typename ::asiopp::base::AsyncFunc<RESULT>>;

		private:
			struct _Container final {
				public:
					bool done = false;
					::asiopp::intern::OptionalElement<RESULT> result;
			};
			using _RESULT_T = RESULT;

			::asiopp::intern::Tracked<_Container> _data;
			::asiopp::base::intern::coroutine_handle<promise_type> _handle;

			bool _run() override {
				if (!this->_data->done) {
					this->_handle();
				}
				return this->_data->done;
			}

			AsyncFunc(::asiopp::base::intern::coroutine_handle<promise_type> handle) : _handle(::std::move(handle)) {}

		public:
			RESULT getResult() {
				return ::std::move(*(this->_data->result.get()));
			}

			friend ::asiopp::base::intern::SimplePromisType<typename ::asiopp::base::AsyncFunc<RESULT>>;
	};
} // namespace asiopp::base
