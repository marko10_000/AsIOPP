#pragma once
#include <asiopp/intern/atomicLocks.hpp>

namespace asiopp::base {
	class Lock final {
		private:
			::asiopp::intern::AtomicLock<true> _fastStateLock = {};
			void *_intern = nullptr;

			static void _destroy(void *entry);

		public:
			Lock() {}
			Lock(const Lock &) = delete;
			Lock(Lock &&lock) {
				lock._fastStateLock.locked([this, &lock]() -> void {
					this->_intern = lock._intern;
					lock._intern = nullptr;
				});
			}
			~Lock() {
				if (this->_intern != nullptr) {
					Lock::_destroy(this->_intern);
				}
			}

			Lock &operator=(const Lock &) = delete;
			Lock &operator=(Lock &&lock) {
				void *tmp;
				lock._fastStateLock.locked([&tmp, &lock]() -> void {
					tmp = lock._intern;
					lock._intern = nullptr;
				});
				this->_fastStateLock.locked([this, &tmp]() -> void {
					void *tmp2 = this->_intern;
					this->_intern = tmp;
					tmp = tmp2;
				});
				if (tmp != nullptr) {
					Lock::_destroy(tmp);
				}
				return *this;
			}
	};
} // namespace asiopp::base
