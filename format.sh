#! /bin/sh

cd "$(dirname "$0")" &&
find src -type f -and '-(' -iname '*.cpp' -or -iname '*.hpp' '-)' -exec clang-format -i '{}' ';'
